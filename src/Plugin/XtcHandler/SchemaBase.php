<?php

namespace Drupal\xtcschema\Plugin\XtcHandler;


use Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase;
use Jkphl\Micrometa\Ports\Parser;
use Jkphl\Micrometa\Ports\Format;

/**
 * Plugin implementation of the xtc_handler for File.
 *
 */
abstract class SchemaBase extends XtcHandlerPluginBase {

  /**
   * @var string
   */
  protected $url;

  /**
   * @var string
   */
  protected $type;

  /**
   * @var Parser
   */
  protected $parser;

  public function values() {
    return $this->content ?? NULL;
  }

  /**
   * @param array $options
   *
   * @return \Drupal\xtc\PluginManager\XtcHandler\XtcHandlerPluginBase
   */
  public function setOptions($options = []): XtcHandlerPluginBase {
    parent::setOptions($options);
    $this->url = $this->profile['fullurl'] ?? '';
    $this->type = $this->profile['schema'] ?? NULL;
    return $this;
  }

  /**
   * @return string
   */
  public function getUrl() {
    return $this->url;
  }

  public function setUrl($url): SchemaBase {
    $this->url = $url;
    return $this;
  }

  protected function initProcess() {
    $this->buildClient();
  }

  protected function buildClient(): SchemaBase {
    $this->parser = new Parser(Format::JSON_LD);
    return $this;
  }

  protected function runProcess() {
    $this->getStream();
  }

  protected function getStream() {
    $content = [];
    if (!empty($this->url)) {
      $item_object_model = $this->parser->__invoke($this->url);
      if (!empty($this->type)) {
        $items = $item_object_model->getItems($this->type);
      }
      else {
        $items = $item_object_model->getItems();
      }
      foreach ($items as $item) {
        $content[] = $item->toObject();
      }
    }
    $this->content = $content;
  }

}
