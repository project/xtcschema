<?php
/**
 * Created by PhpStorm.
 * User: aisrael
 * Date: 2019-04-18
 * Time: 19:23
 */

namespace Drupal\xtcschema\Plugin\XtcHandler;


/**
 * Plugin implementation of the xtc_handler.
 *
 * @XtcHandler(
 *   id = "schema_get",
 *   label = @Translation("Schema Get for XTC"),
 *   description = @Translation("Schema Get for XTC description.")
 * )
 */
class SchemaGet extends SchemaBase {


}
